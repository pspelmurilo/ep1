#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <bits/stdc++.h>

using namespace std;

class Embarcacao{
	private:
		string nome;
		int tamanho;
		string direcao;
	public:
		Embarcacao();
		Embarcacao(string nome,int tamanho, string direcao);
		~Embarcacao();
		void set_nome(string nome);
		string get_nome();
		void set_tamanho(int tamanho);
		int get_tamanho();
		void set_direcao(string direcao);
		string get_direcao();
		virtual void ataca_embarcacao(int linha, int coluna);
		virtual bool checa_coordenada(int linha, int coluna);
		virtual int get_vida_total();
		virtual int get_vida_especifica(int linha, int coluna);
};
#endif
