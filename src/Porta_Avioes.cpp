#include "Porta_Avioes.hpp"
#include <bits/stdc++.h>

using namespace std;

Porta_Avioes :: Porta_Avioes(){
	set_nome("Porta-Avioes");
	set_tamanho(4);
	for(int i=0;i<4;++i){
		x[i] = -1;
		y[i] = -1;
	}
	set_vidas(1);
}

Porta_Avioes :: Porta_Avioes(int linha,int coluna, string direcao){
	set_nome("Porta-Avioes");
	set_tamanho(4);
	x[0] = linha;
	x[1] = linha;
	x[2] = linha;
	x[3] = linha;
	y[0] = coluna;
	y[1] = coluna;
	y[2] = coluna;
	y[3] = coluna;
	if(direcao[0] == 'e'){
		y[1] = coluna - 1;
		y[2] = coluna - 2;
		y[3] = coluna - 3;
	}
	else if(direcao[0] == 'd'){
		y[1] = coluna + 1;
		y[2] = coluna + 2;
		y[3] = coluna + 3;
	}
	else if(direcao[0] == 'c'){
		x[1] = linha - 1;
		x[2] = linha - 2;
		x[3] = linha - 3;
	}
	else{
		x[1] = linha + 1;
		x[2] = linha + 2;
		x[3] = linha + 3;
	}
	set_vidas(1);
}

Porta_Avioes :: ~Porta_Avioes(){

}

void Porta_Avioes :: set_vidas(int vida){
	for(int i=0;i<4;i++){
		vidas[i].first = vida;
		vidas[i].second.first = x[i];
		vidas[i].second.second = y[i];
		vida_total += vida;
	}
}

int Porta_Avioes :: get_vida_total(){
	return vida_total;
}

void Porta_Avioes::ataca_embarcacao(int linha,int coluna){
	for(int i=0;i<4;++i){
		if(linha == x[i] && coluna == y[i]){
			if(vidas[i].first>0){
				srand(time(0));
				int random = rand() % 2;
				if(random==1){ 
					vidas[i].first--;
					vida_total--;
					if(vida_total==3){
						cout << "Voce destruiu uma parte de um Porta-Avioes, procure as outras 3!" << endl;
					}
					else if(vida_total==2){
						cout << "Voce destrui a segunda parte de um Porta-Avioes, procure as outras 2!" << endl;
					}
					else if(vida_total==1){
						cout << "Voce destruiu a terceira parte de um Porta-Avioes, procure a ultima!" << endl;
					}
					else if(vida_total==0){
						cout << "Voce destruiu completamente um Porta-Avioes!" << endl;
					}
				}
				else{
					cout << "Voce acertou uma parte de um Porta-Avioes, porem nao a destruiu! :(" << endl;
				}
			}
			else if(vida_total==3){
				cout << "Voce ja destruiu essa parte do Porta-Avioes, tente acertar as outras 3!" << endl;
			}
			else if(vida_total==2){
				cout << "Voce ja destruiu essa parte do Porta-Avioes, tente acertar as outras 2!" << endl;
			}
			else if(vida_total==1){
				cout << "Voce ja destruiu essa parte do Porta-Avioes, tente acertar as outras 1!" << endl;
			}
			else{
				cout << "Voce ja destruiu esse Porta-Avioes, procure outra(s) embarcacao(oes)!" << endl;
			}
		}
	}
}

bool Porta_Avioes::checa_coordenada(int linha, int coluna){
	for(int i=0;i<4;++i){
		if(linha == x[i] && coluna == y[i]){
			return true;
		}
	}
	return false;
}

int Porta_Avioes::get_vida_especifica(int linha, int coluna){
	for(int i=0;i<4;++i){
		if(linha==x[i] && coluna==y[i]){
			return vidas[i].first;
		}
	}
	return 0;
}