#include "Submarino.hpp"
#include <bits/stdc++.h>

using namespace std;

Submarino ::Submarino(){
	set_nome("Submarino");
	set_tamanho(2);
	x[0] = -1;
	y[0] = -1;
	x[1] = -1;
	y[1] = -1;
	set_vidas(2);
}

Submarino::Submarino(int linha,int coluna,string direcao){
	set_nome("Submarino");
	set_tamanho(2);
	set_direcao(direcao);
	
	x[0] = linha;
	y[0] = coluna;
	x[1] = linha;
	y[1] = coluna;
	
	if(direcao[0] == 'e')
		y[1] = coluna - 1;
	else if(direcao[0] == 'd')
		y[1] = coluna + 1;
	else if(direcao[0] == 'c')
		x[1] = linha - 1;
	else
		x[1] = linha + 1;
	set_vidas(2);

}

Submarino::~Submarino(){

}

void Submarino::set_vidas(int vida){
	for(int i=0;i<2;++i){
		vidas[i].first = vida;
		vidas[i].second.first = x[i];
		vidas[i].second.second = y[i];
		vida_total += vida;
	}
}

int Submarino::get_vida_total(){
	return vida_total;
}

void Submarino::ataca_embarcacao(int linha,int coluna){
	for(int i=0;i<2;++i){
		if(linha == x[i] && coluna == y[i]){
			if(vidas[i].first>0){
				vidas[i].first--;
				vida_total--;
				if(vidas[i].first>0 && vida_total>2){
					cout << "Voce acertou uma parte de um submarino, acerte novamente para destrui-la!" << endl;
				}
				else if(vidas[i].first>0){
					cout << "Voce acertou a segunda parte de um submarino, acerte novamente para destrui-la!" << endl;
				}
				else if(vidas[i].first==0 && vida_total > 0){
					cout << "Voce destruiu uma parte de um submarino, procure a outra!" << endl;
				}
				else{
					cout << "Voce destruiu completamente um submarino!" << endl;
				}
			}
			else if(vidas[i].first==0){
				cout << "Voce ja destruiu essa parte do submarino, tente acertar a outra" << endl;
			}
			else{
				cout << "Voce ja destruiu esse submarino, procure outras embarcacoes" << endl;
			}
		}
	}
}

bool Submarino::checa_coordenada(int linha, int coluna){
	for(int i=0;i<2;++i){
		if(linha == x[i] && coluna == y[i]){
			return true;
		}
	}
	return false;
}

int Submarino::get_vida_especifica(int linha, int coluna){
	for(int i=0;i<2;++i){
		if(linha==x[i] && coluna==y[i]){
			return vidas[i].first;
		}
	}
	return 0;
}