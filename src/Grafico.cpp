#include "Grafico.hpp"
#include <bits/stdc++.h>

Grafico::Grafico(){
	for(int i=0;i<13;i++){
		for(int j=0;j<13;j++){
			visitado[i][j] = false;
			sigla[i][j].first = 'A';
			sigla[i][j].second = 0;
		}

	}
}

Grafico::~Grafico(){


}

void Grafico::set_visitado(int x, int y){
	visitado[x][y] = true;
}

void Grafico::set_sigla(int x,int y,int num, char letra){
	sigla[x][y].first = letra;
	sigla[x][y].second = num;
}

void Grafico::print_mapa(){
	cout << "Mapa Inimigo:" << endl;
	cout << "    0   1   2   3   4   5   6   7   8   9   10  11  12" << endl;
	for(int i=0;i<13;i++){
		if(i>=10){
			cout << i << " ";
		}
		else{
			cout << i << "  ";
		}
		for(int j=0;j<13;j++){
			cout << "[";
			if(visitado[i][j]){
				cout << sigla[i][j].first << sigla[i][j].second;
			}
			else{ 
				cout << "  ";
			}
			cout << "]";
		}
		cout << endl;
	}

}