#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include "Embarcacao.hpp"
#include <bits/stdc++.h>

using namespace std;

class Porta_Avioes : public Embarcacao{
	private:
		int x[4];
		int y[4];
		pair<int,pair<int,int>> vidas[4];
		int vida_total = 0;
		bool derruba_porta_avioes = false;
	public:
		Porta_Avioes();
		Porta_Avioes(int linha,int coluna, string direcao);
		~Porta_Avioes();
		void set_vidas(int vida);
		int get_vida_total();
		void ataca_embarcacao(int linha, int coluna);
		bool checa_coordenada(int linha, int coluna);
		int get_vida_especifica(int linha, int coluna);
};

#endif
