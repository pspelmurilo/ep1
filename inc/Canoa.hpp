#ifndef CANOA_HPP
#define CANOA_HPP

#include "Embarcacao.hpp"
#include <bits/stdc++.h>

class Canoa : public Embarcacao{
	private:
		int x,y;
		pair<int, pair<int,int>> vidas;
		int vida_total;
	public:
		Canoa();
		Canoa(int linha, int coluna);
		~Canoa();
		void set_vidas(int vida);
		int get_vida_total();
		void ataca_embarcacao(int linha, int coluna);
		bool checa_coordenada(int linha, int coluna);
		int get_vida_especifica(int linha, int coluna);
};

#endif
