#include <iostream>
#include <string>
#include <fstream>
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "Porta_Avioes.hpp"
#include "Grafico.hpp"


using namespace std;
int main(){
	while(1){
		int opcao;
		system("clear");
		cout << "Bem-Vindos ao jogo Batalha Naval!" << endl;
		cout << "Digite uma das opções abaixo:" << endl;
		cout << "1 - Jogar Batalha Naval" << endl;
		cout << "2 - Sobre o Jogo" << endl;
		cout << "3 - Encerrar programa" << endl;
		cin >> opcao;
		scanf("%*c");
		if(opcao <1 || opcao > 3){
			cout << "Opção inválida digite novamente" << endl;
			cin >> opcao;
		}
		system("clear");
		if(opcao == 1){
			vector<Embarcacao*> player1;
			vector<Embarcacao*> player2;
			ifstream mapa;
			system("clear");
			cout << "Digite o número do mapa no qual deseja jogar(1, 2 ou 3): ";
			int map;
			cin >> map;
			while(1){
				switch(map){
					case 1:
						mapa.open("doc/map_1.txt");
						break;
					case 2:
						mapa.open("doc/map_2.txt");
						break;
					case 3:
						mapa.open("doc/map_3.txt");
						break;
					default:
						cout <<"Não existe esse mapa." << endl;
						cout <<"Digite novamente o numero do mapa(1, 2 ou 3):" ;
						cin >> map;
						break;
				}
				if(mapa.is_open())
					break;
			}
			scanf("%*c");
			cout << "Aperte ENTER para começar o jogo";
			scanf("%*c");
			system("clear");
			string linha;
			if(mapa.is_open()){
				int k = 0;
				while(getline(mapa,linha)){
					if(linha[0]>='0' && linha[0] <='9'){
						int i = 0;
						string aux = "";
						while(linha[i] != ' '){
							aux += linha[i];
							i++;
						}
						i++;
						int x = stoi(aux);
						aux = "";
						while(linha[i] != ' '){
							aux += linha[i];
							i++;
						}
						i++;
						int y = stoi(aux);
						aux = "";
						while(linha[i] != ' '){
							aux += linha[i];
							i++;
						}
						i++;
						string embarcacao = aux;
						aux = "";
						while(linha[i] != '\0'){
							aux += linha[i];
							i++;
						}
						string direcao = aux;
						if(k<12){
							if(embarcacao == "canoa"){
								Canoa *canoatemp = new Canoa(x,y);
								player1.push_back(canoatemp);
							}
							else if(embarcacao == "submarino"){
								Submarino *submarinotemp = new Submarino(x,y,direcao);
								player1.push_back(submarinotemp);
							}
							else if(embarcacao == "porta-avioes"){
								Porta_Avioes *porta_avioestemp = new Porta_Avioes(x,y,direcao);
								player1.push_back(porta_avioestemp);
							}

						}
						else{
							if(embarcacao == "canoa"){
								Canoa *canoatemp = new Canoa(x,y);
								player2.push_back(canoatemp);
							}
							else if(embarcacao == "submarino"){
								Submarino *submarinotemp = new Submarino(x,y,direcao);
								player2.push_back(submarinotemp);
							}
							else if(embarcacao == "porta-avioes"){
								Porta_Avioes *porta_avioestemp = new Porta_Avioes(x,y,direcao);
								player2.push_back(porta_avioestemp);
							}
						}
						k++;
					
					}
				}
			}
			Grafico graficoplayer1;
			Grafico graficoplayer2;
			while(!player1.empty() && !player2.empty()){
				int stop = 1;
				int i = 0; //Valor das posições percorridas no vector
				int x1,y1;
				cout << "Turno do Player 1:" << endl;
				graficoplayer1.print_mapa();
				cout << "Digite a posição onde deseja jogar a bomba(Linha e Coluna): ";
				cin >> x1 >> y1;
				while(x1 > 12 || y1 > 12 || x1 < 0 || y1 < 0){
					cout << "Posição inválida digite novamente:";
					cin >> x1 >> y1; 
				}
				graficoplayer1.set_visitado(x1,y1);
				scanf("%*c");
				bool ataque = false;
				for(auto x: player2){
					ataque = x -> checa_coordenada(x1,y1);
					if(ataque){
					x -> ataca_embarcacao(x1,y1);
						int num = x -> get_vida_especifica(x1,y1);
						string nome = x-> get_nome();
						char letra = nome[0];
						graficoplayer1.set_sigla(x1,y1,num,letra);
						stop = x -> get_vida_total();
						break;
					}
					i++;
				}
				if(!ataque){
					cout << "Só foi encontrado agua na posição " << x1 << " " << y1 << endl;
				}
				scanf("%*c");
				system("clear");
				cout << "Fim do turno do Player 1" << endl;
				graficoplayer1.print_mapa();
				cout << "Aperte ENTER para continuar";
				scanf("%*c");
				system("clear");
				if(stop == 0){
					player2.erase(player2.begin()+i);
					if(player2.empty()){
						cout << "PLAYER 1 DESTRUIU TODAS AS EMBARCAÇÕES DO PLAYER 2" << endl;
						cout << "PLAYER 1 WINS! :)" << endl;
						cout << "PLAYER 2 LOSE! :(" << endl;
						scanf("%*c");
						break;
					}
				}
				stop = 1;
				i = 0;
				int x2,y2;
				cout << "Turno do Player 2:" << endl;
				graficoplayer2.print_mapa();
				cout << "Digite a posição onde deseja jogar a bomba(Linha e Coluna): ";
				cin >> x2 >> y2;
				while(x2 > 12 || y2 > 12 || x2 < 0 || y2 < 0){
					cout << "Posição inválida digite novamente:";
					cin >> x1 >> y1; 
				}
				scanf("%*c");
				ataque = false;
				graficoplayer2.set_visitado(x2,y2);
				for(auto x: player1){
					ataque = x ->checa_coordenada(x2,y2);
					if(ataque){
						x -> ataca_embarcacao(x2,y2);
						int num = x->get_vida_especifica(x2,y2);
						string nome = x-> get_nome();
						char letra = nome[0];
						graficoplayer2.set_sigla(x2,y2,num,letra);
						stop = x ->get_vida_total();
						break;
					}	
					i++;
				}
				if(!ataque){
					cout << "Só foi encontrado agua na posição " << x2 << " " << y2 << endl;
				}
				scanf("%*c");
				system("clear");
				cout << "Fim do turno do Player 2" << endl;
				graficoplayer2.print_mapa();
				cout << "Aperte ENTER para continuar";
				scanf("%*c");
				system("clear");
				if(stop == 0){
					player1.erase(player1.begin()+i);
					if(player1.empty()){
						cout << "PLAYER 2 DESTRUIU TODAS AS EMBARCAÇÕES DO PLAYER 1" << endl;
						cout << "PLAYER 2 WINS! :)" << endl;
						cout << "PLAYER 1 LOSE! :(" << endl;
						cout << "Aperte ENTER para voltar para o menu inicial" << endl;
						scanf("%*c");
						break;
					}
				}
			}
		}
		else if(opcao == 2){
			cout << "O jogo Batalha Naval é composto por 2 jogadores e funciona da seguinte forma:" << endl;
			cout << "Ao entrar no jogo precisa ser selecionado 1 entre os 3 mapas disponíveis, cada mapa define posições ";
			cout << "das embarcações para cada player, há 3 tipos de embarcação:" << endl;
			cout << "	Canoa: Ocupa 1 casa e ao ser acertada é destruida imediatamente, no total são 6 canoas para cada jogador." << endl;
			cout << "	Submarino: Ocupa 2 casas e requer que cada casa seja acertada duas vezes para que seja destruida, no total são 4 por jogador." << endl;
			cout << "	Porta-Avioes: Ocupa 4 casas e ao acertar uma casa de porta-avioes será decidido aleatoriamente se essa casa será destuida ou não" << endl;
			cout << "Após definidas as embarcações de acordo com o mapa escolhido, cada player terá um turno em ";
			cout << "que ele tenta acertar alguma embarcação escrevendo a linha e a coluna que o player deseja jogar";
			cout << " a bomba, a coordenada que o player joga a bomba recebe a primeira a letra do que foi achado naquela";
			cout << " posição e a vida daquela embarcação caso tenha achado uma, caso tenha achado apenas água será ";
			cout << "representao por um A e por um 0" << endl;
			cout << "O player que destruir todas as embarcações do inimigo ganha." << endl;
			cout << "Aperte ENTER para voltar para o menu principal";
			scanf("%*c");
		}
		else if(opcao == 3)
			break;
	}
    return 0;
}
