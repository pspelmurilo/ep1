#include "Canoa.hpp"
#include <bits/stdc++.h>

using namespace std;

Canoa::Canoa(){
	set_nome("Canoa");
	set_tamanho(1);
	x = -1;
	y = -1;
	set_vidas(1);
}
Canoa::Canoa(int linha, int coluna){
	set_nome("Canoa");
	set_tamanho(1);
	x = linha;
	y = coluna;
	set_vidas(1);
}
Canoa::~Canoa(){

}

void Canoa::set_vidas(int vida){
	vidas.first = vida;
	vidas.second.first = x;
	vidas.second.second = y;
	vida_total += vida;
}

int Canoa::get_vida_total(){
	return vida_total;
}

void Canoa::ataca_embarcacao(int linha,int coluna){
	if(linha == x && coluna == y){
		if(vidas.first>0){
			vidas.first--;
			vida_total--;
			cout << "Você destruiu uma canoa!\n";
		}
		else{
			cout << "Você já destruiu essa canoa!" << endl;
		}
	}


}

bool Canoa::checa_coordenada(int linha,int coluna){
	if(linha == x && coluna == y){
		return true;
	}
	return false;
}
int Canoa::get_vida_especifica(int linha, int coluna){
	if(linha==x && coluna==y){
		return vidas.first;
	}
	else
		return 0;
}