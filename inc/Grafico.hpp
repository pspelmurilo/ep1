#ifndef GRAFICO_HPP
#define GRAFICO_HPP

#include <bits/stdc++.h>

using namespace std;

class Grafico{
	private:
		bool visitado[13][13];
		pair<char,int> sigla[13][13];
	public:
		Grafico();
		~Grafico();
		void set_visitados_padrao();
		void set_visitado(int x, int y);
		void set_siglas_padrao();
		void set_sigla(int x, int y, int num, char );
		void print_mapa();
};

#endif