# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

&nbsp;&nbsp;Recomenda-se construir o corpo do README com no mínimo no seguinte formato:

* Descrição do projeto
* Instruções de execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

* Fique avontade para manter o README da forma que achar melhor, mas é importante manter ao menos as informações descritas acima para, durante a correção, facilitar a avaliação.
* Divirtam-se mas sempre procurando atender aos critérios de avaliação informados na Wiki, ela definirá a nota a ser atribuída ao projeto.

## Descrição do projeto

Como dito acima, o projeto é um jogo similar ao jogo Batalha Naval. No projeto criado dois jogadores jogam um contra o outro com a intenção de destruir todos os barcos do outro jogador.

## Instruções de execução

Ao executar o programa, será apresentado um menu inicial com 3 opções: Começar a partida, Informações sobre o jogo e a opção de encerrar o programa. Quando é iniciada uma partida, o jogo da opção de 3 mapas pré-definidos que posicionam as embarcações de cada jogador de acordo com os arquivos de texto disponibilizados. Ao ser escolhido o mapa, a partida se inicia e cada jogador tem um turno por rodada, em cada turno é apresentado um mapa, inicialmente vazio, que mostra as posições que podem ser atacadas pelo jogador, no momento em que o jogador ataca uma coordenada, aquela coordenada passa a ser representada por uma letra e um número, que representa o que foi encontrado naquela coordenada e o número de vidas daquele ponto, sendo que a água é representada por 'A', a canoa por 'C', o porta-avioes por 'P', e o submarino por 'S'. No momento em que um jogador destrói todas as embarcações do jogador inimigo, o jogo é encerrado.
