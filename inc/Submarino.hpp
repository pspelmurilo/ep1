#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "Embarcacao.hpp"
#include <bits/stdc++.h>

class Submarino : public Embarcacao{
	private:
		int x[2];
		int y[2];
		pair<int,pair<int,int>> vidas[2];
		int vida_total = 0;
	public:
		Submarino();
		Submarino(int linha,int coluna, string direcao);
		~Submarino();
		void set_vidas(int vida);
		int get_vida_total();
		void ataca_embarcacao(int linha, int coluna);
		bool checa_coordenada(int linha, int coluna);
		int get_vida_especifica(int linha, int coluna);
		
};

#endif
