#include "Embarcacao.hpp"
#include <bits/stdc++.h>

using namespace std;

Embarcacao::Embarcacao(){
	set_nome("");
	set_tamanho(0);
	set_direcao("");

}
Embarcacao::Embarcacao(string nome, int tamanho, string direcao){
	set_nome(nome);
	set_tamanho(tamanho);
	set_direcao(direcao);

}
Embarcacao::~Embarcacao(){

}

void Embarcacao::set_nome(string nome){
	this -> nome = nome;
}

string Embarcacao::get_nome(){
	return nome;
}

void Embarcacao::set_tamanho(int tamanho){
	this -> tamanho = tamanho;
}

int Embarcacao::get_tamanho(){
	return tamanho;
}

void Embarcacao::set_direcao(string direcao){
	this -> direcao = direcao;
}
string Embarcacao::get_direcao(){
	return direcao;
}
bool Embarcacao::checa_coordenada(int linha, int coluna){
	return false;
}

void Embarcacao::ataca_embarcacao(int linha, int coluna){}

int Embarcacao::get_vida_total(){
	return 0;
}

int Embarcacao::get_vida_especifica(int linha, int coluna){
	return 0;
}
